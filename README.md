# assignment-2-trivia-game

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A trivia game made to learn Vue 3.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
- Clone or fork the project
- Navigate to the project folder
- Run "npm install" terminal command
```

## Usage

```
- Run "npm run dev" to start local server on which you can use the trivia game.
```

## API
https://assignment-2-trivia-game-api.herokuapp.com/trivia

## Maintainers

[@noitcereon](https://github.com/noitcereon)
[@4ndersom](https://gitlab.com/4ndersom)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Thomas Andersen
