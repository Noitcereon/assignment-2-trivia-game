const BASE_API_URL = "https://opentdb.com/api.php"

function generateApiUrlWithSettings(noOfQuestions=10, difficulty="medium", categoryId = 20){
    const fullApiUrl = `${BASE_API_URL}?amount=${noOfQuestions}&category=${categoryId}&difficulty=${difficulty}`;
    console.info(fullApiUrl);
    return fullApiUrl;
}
/**
 * 
 * @param {Number} noOfQuestions The number of questions to generate.
 * @param {String} difficulty (Optional) Default is medium difficulty. Options are "easy", "medium", "hard"
 * @param {Number} categoryId (Optional) if passed in, it will only select questions from the category specified
 * @returns a Promise containing an array of questions.
 */
export async function getQuestionsFromApiAsync(noOfQuestions,  difficulty="medium", categoryId=-1){
    const apiUrl = generateApiUrlWithSettings(noOfQuestions, difficulty, categoryId);
    const response = await fetch(apiUrl);
    const questionsResponseObject = await response.json();
    const questionsArray = questionsResponseObject.results;
    return questionsArray;
}

export async function getAllQuestionCategories(){
    // TODO: make getAllQuestionCategories
    // https://opentdb.com/api_category.php (url to use)
}
