import { createRouter, createWebHistory } from "vue-router";
import Start from "./views/Start.vue"
import Game from "./views/Game.vue"
import Result from "./views/Result.vue"

const isDev = import.meta.env.VITE_MODE === "development";
const baseUrl = isDev ? "/" : "/assignment-2-trivia-game/";

const routes = [
    {
        name: "Home",
        path: baseUrl,
        component: Start
    },
    {
        name: "Game",
        path: "/Game",
        component: Game
    },
    {
        path: "/Result",
        component: Result
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})