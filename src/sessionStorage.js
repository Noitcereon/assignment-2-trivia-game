
const user = "user";
const difficulty = "difficulty";
const questionData = "questionData";
const userAnswers = "userAnswers";

export const storeKeys = {
    user, difficulty, questionData, userAnswers
};