import { storeKeys } from "./sessionStorage";
const BASE_API_URL = "https://noit-noroff-assignment-api.herokuapp.com/trivia";
const API_KEY = "105E23DD288A4ED9B869F385818AE928"; // API keys should be retrieved from environment variables normally

export async function createUserAsync(userName) {
    if (await userNameExistsAsync(userName)) {
        console.warn("User already exists");
        return null;
    }
    // #region API request to create user.
    const requestConfig = {
        method: 'POST',
        headers: {
            "X-API-Key": API_KEY,
            "content-type": "application/json",
        },
        body: JSON.stringify({
            username: userName,
            highScore: 0
        })
    }
    const response = await fetch(`${BASE_API_URL}`, requestConfig);
    if (response.ok) {
        // convert response body to object.
        const createdUser = await response.json();
        return createdUser;
    }
    // #endregion
}
export async function getUserByUserNameAsync(userName) {
    const response = await fetch(`${BASE_API_URL}?username=${userName}`);
    const userArray = await response.json();
    if (userArray.length === 1) {
        return userArray[0];
    }
    return {}; // return empty object if user doesn't exist.
}
export async function userNameExistsAsync(userName) {
    try {
        const user = await getUserByUserNameAsync(userName);
        if (user.username === userName) {
            return true; // user exists
        }
        return false; // user doesn't exist
    }
    catch (error) {
        console.error(error);
    }
}
export async function updateHighScore(score) {
    const user = await JSON.parse(sessionStorage.getItem(storeKeys.user));
    if (score > Number.parseInt(user.highScore)) {
        fetch(`${BASE_API_URL}/${user.id}`, {
            method: "PATCH",
            headers: {
                "X-API-Key": API_KEY,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ highScore: score }),
        });
        return;
    }
    console.log("Score was lower or equal to current highScore");
}

