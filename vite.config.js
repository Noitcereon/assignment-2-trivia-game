import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '')
  const base = env.VITE_MODE === "development" ? "/" : "/assignment-2-trivia-game/";
  console.log("Mode: " + env.VITE_MODE);
  return {
    plugins: [vue()],
    base: base
  }
})
